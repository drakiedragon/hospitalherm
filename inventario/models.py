from django.db import models


# Create your models here.

class Registrado(models.Models):
    idRegistro = models.AutoField(primary_key=True, unique=True)
    idproducto = models.Charfield(max_lengh=100, min_lengh=2, )
    cantidadProducto = models.Charfield(min=1)
    pertencia1 = models.Charfield(max_lenght=50, blank=True, null=True)
    pertenencia2 = models.Charfield(max_lenght=50, blank=True, null=True)

    def __str__(self):
        return self.idRegistro
